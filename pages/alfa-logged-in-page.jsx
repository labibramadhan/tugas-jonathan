var AlfaLoggedIn = () => {
    jQuery(function ($) {
        $('#worldWideModal').modal('show');
    });
    return (
        <>
            <div style={{ backgroundImage: 'url(assets/alfa-cart-backgound.png)', backgroundRepeat: "no-repeat", height: "100vh" }}>
                
            </div>
            {/* Modal Bootstrap World Wide */}
            <div className="modal fade" id="worldWideModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document" style={{ height: "100%", maxWidth: "50%" }}>
                    <div className="modal-content">
                        <div className="modal-body">
                            <div className="col-12">
                                <div className="card">
                                    <img src="assets/alfa-cart-world-wide.png" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {/* End Modal Bootstrap World Wide */}
        </>
    )
}