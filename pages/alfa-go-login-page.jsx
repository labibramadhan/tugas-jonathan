var AlfaGoLogin = () => {
    return (
        <>
            <div className="container">
                <div className="row py-5">
                    <div className="col-4">
                        <div className="card"
                            style={{ width: "100%", borderRadius: "10px", border: "none", boxShadow: "0 1px 11px 0 rgba(233, 233, 233, 0.7)" }}>
                            <img src="assets/icon-alfa-go.png" style={{ width: "100px", margin: "auto", paddingTop: "40px" }} />
                            <div className="card-body text-center justify-content-center">
                                <p className="card-title text-center pb-2"><span style={{ fontWeight: 800, fontSize: "2rem", paddingRight: "5px" }}>+100</span>Poin</p>
                                <RRD.Link to="/home">
                                    <button
                                        className="btn btn-primary text-center"
                                        style={{
                                            backgroundColor: "#005ca8",
                                            borderColor: "#005ca8",
                                            borderRadius: "10px",
                                            padding: "10px 15px",
                                            margin: "auto"
                                        }}>
                                        Login AlfaCart
                                </button>
                                </RRD.Link>
                                <h2 className="pt-4" style={{ fontSize: "16px", fontWeight: 600, paddingBottom: "8px" }}>
                                    BERBAGI BERKAT DENGAN ALFACART
                                </h2>
                                <p style={{ paddingBottom: "20px" }}>
                                    Perlakuan Kecilmu menghasilkan berjuta cerita.<br/>Terima Kasih.
                                </p>
                                {/* Footer */}
                                <div
                                    style={{
                                        position: "relative",
                                    }}
                                >
                                    <div className="container">
                                        <div
                                            style={{
                                                display: "inline-block",
                                                display: "table",
                                                margin: "auto",
                                            }}
                                        >
                                            <ul className="list-unstyled list-inline">
                                                <li className="list-inline-item">
                                                    <a href="https://www.youtube.com/channel/UCj-E9ANupFZVYX-13WHHVLw" target="_blank">
                                                        <img src="assets/icon-youtube.png" />
                                                    </a>
                                                </li>
                                                <li className="list-inline-item">
                                                    <a href="https://twitter.com/alfacartID" target="_blank">
                                                        <img src="assets/icon-twitter.png" />
                                                    </a>
                                                </li>
                                                <li className="list-inline-item">
                                                    <a href="https://www.instagram.com/alfacartID" target="_blank">
                                                        <img src="assets/icon-instagram.png" />
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                {/* Footer */}
                            </div>
                        </div>
                    </div>
                    <div className="col-4">
                        <div className="card"
                            style={{ width: "100%", borderRadius: "10px", border: "none", boxShadow: "0 1px 11px 0 rgba(233, 233, 233, 0.7)" }}>
                            <img src="assets/icon-alfa-go.png" style={{ width: "100px", margin: "auto", paddingTop: "40px" }} />
                            <div className="card-body text-center justify-content-center">
                                <p className="card-title text-center pb-2"><span style={{ fontWeight: 800, fontSize: "2rem", paddingRight: "5px" }}>+100</span>Poin</p>
                                <RRD.Link to="/home">
                                    <button
                                        className="btn btn-primary text-center"
                                        style={{
                                            backgroundColor: "#005ca8",
                                            borderColor: "#005ca8",
                                            borderRadius: "10px",
                                            padding: "10px 15px",
                                            margin: "auto"
                                        }}>
                                        Login AlfaCart
                                </button>
                                </RRD.Link>
                                <h2 className="pt-4" style={{ fontSize: "16px", fontWeight: 600, paddingBottom: "8px" }}>
                                    YEY VOUCHER ALFAPAY MU AKAN LANGSUNG MASUK KE ALFACART YA!
                                </h2>
                                <p style={{ paddingBottom: "20px" }}>
                                    Check out dan nikmati kerja kerasmu!!
                                </p>
                                {/* Footer */}
                                <div
                                    style={{
                                        position: "relative",
                                    }}
                                >
                                    <div className="container">
                                        <div
                                            style={{
                                                display: "inline-block",
                                                display: "table",
                                                margin: "auto",
                                            }}
                                        >
                                            <ul className="list-unstyled list-inline">
                                                <li className="list-inline-item">
                                                    <a href="https://www.youtube.com/channel/UCj-E9ANupFZVYX-13WHHVLw" target="_blank">
                                                        <img src="assets/icon-youtube.png" />
                                                    </a>
                                                </li>
                                                <li className="list-inline-item">
                                                    <a href="https://twitter.com/alfacartID" target="_blank">
                                                        <img src="assets/icon-twitter.png" />
                                                    </a>
                                                </li>
                                                <li className="list-inline-item">
                                                    <a href="https://www.instagram.com/alfacartID" target="_blank">
                                                        <img src="assets/icon-instagram.png" />
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                {/* Footer */}
                            </div>
                        </div>
                    </div>
                    <div className="col-4">
                        <div className="card" style={{ width: "100%", borderRadius: "10px", border: "none", boxShadow: "0 1px 11px 0 rgba(233, 233, 233, 0.7)" }} >
                            {/* Navbar */}
                            <div className="container pt-5">
                                <div className="row">
                                    <div className="col-6 align-self-center" style={{ display: "inline-block", verticalAlign: "middle" }}>
                                        <RRD.Link to="/home">
                                            <img src="assets/bingo-alfa-go.png" style={{ width: "100px", margin: "auto" }} />
                                        </RRD.Link>
                                    </div>
                                    <div className="col-6" style={{ display: "inline-block", float: "right" }}>
                                        <img src="assets/icon-alfa-go.png" style={{ width: "100px" }} />
                                    </div>
                                </div>
                            </div>

                            {/* End Of Navbar */}
                            <div className="card-body">
                                <h2 className="text-center" style={{ fontWeight: 800 }}>+100<span style={{ fontWeight: 500, fontSize: "10px" }}>POIN</span></h2>
                                <hr />
                                <div className="py-4">
                                    <img src="assets/alfa-pay@2x.png" style={{ margin: "auto", width: "80px" }} />
                                </div>
                                <div id="login-form" className="row text-center justify-content-center">
                                    <h2 className="text-center">Log In</h2>
                                    <p style={{ fontSize: "14px" }}>Untuk tetap terhubung dengan kami, harap masuk dengan nomor telepon dan kata sandi</p>
                                    <form>
                                        <div className="form-group">
                                            <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" />
                                        </div>
                                        <div className="form-group">
                                            <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Password" />
                                        </div>
                                        <button data-toggle="modal" data-target="#exampleModal" className="btn btn-primary" style={{ borderRadius: "10px", backgroundColor: "#005ca8", borderColor: "#005ca8" }}>Submit</button>
                                    </form>
                                    <p style={{ fontSize: "12px", paddingTop: "20px" }}>Belum punya akun? <span style={{ color: "#2196f3" }}>Daftar sekarang</span></p>
                                </div>
                                {/* Footer */}
                                <div
                                    style={{
                                        position: "relative",
                                    }}
                                >
                                    <div className="container">
                                        <div
                                            style={{
                                                display: "inline-block",
                                                display: "table",
                                                margin: "auto",
                                            }}
                                        >
                                            <ul className="list-unstyled list-inline">
                                                <li className="list-inline-item">
                                                    <a href="https://www.youtube.com/channel/UCj-E9ANupFZVYX-13WHHVLw" target="_blank">
                                                        <img src="assets/icon-youtube.png" />
                                                    </a>
                                                </li>
                                                <li className="list-inline-item">
                                                    <a href="https://twitter.com/alfacartID" target="_blank">
                                                        <img src="assets/icon-twitter.png" />
                                                    </a>
                                                </li>
                                                <li className="list-inline-item">
                                                    <a href="https://www.instagram.com/alfacartID" target="_blank">
                                                        <img src="assets/icon-instagram.png" />
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                {/* Footer */}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {/* Modal */}
            <div className="modal fade" id="exampleModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document" style={{ paddingTop: "50px", maxWidth: "700px", margin: "auto" }}>
                    <div className="modal-content" >
                        <div className="modal-body">
                            {/* Body */}
                            <div className="alfacart-world-wide text-center py-5">
                                <h1 className="pb-5">Alfacart World Wide</h1>
                                <img src="assets/alfa-cart-world-wide.png" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}