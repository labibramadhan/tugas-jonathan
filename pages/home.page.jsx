var HomePage = () => {
  return (
    <>
      <div className="container">
        <div style={{ marginTop: "50px" }}>
          <div style={{ display: "inline-block", verticalAlign: "middle" }}>
            <RRD.Link to="/home">
              <img src="assets/bingo-alfa-go.png" />
            </RRD.Link>
          </div>
          <div style={{ display: "inline-block", float: "right" }}>
            <ul className="list-unstyled list-inline">
              <li className="list-inline-item">
                <a href="#feat-alfago">ALFA GO</a>
              </li>
              <li className="list-inline-item" style={{ paddingLeft: "50px" }}>
                <a href="#feat-alfaworldwide">ALFA WORLD WIDE</a>
              </li>
              <li className="list-inline-item" style={{ paddingLeft: "50px" }}>
                <a href="#feat-alfapay">ALFA PAY</a>
              </li>
            </ul>
          </div>
        </div>

        <div
          className="row"
          style={{
            marginLeft: 0,
            marginRight: 0,
            marginTop: "100px",
            backgroundImage: "url(assets/box-bg.png)",
            backgroundSize: "contain",
            backgroundRepeat: "no-repeat",
          }}
        >
          <div className="col-xs-4">
            <img
              src="assets/big-map-pointer.png"
              style={{
                position: "relative",
                top: "-50px",
              }}
            />
          </div>
          <div
            className="col-xs-4"
            style={{
              color: "white",
              position: "relative",
              top: "20px",
            }}
          >
            <h1 style={{ fontSize: "6em", marginBottom: 0 }}>BINGO</h1>
            <h2
              style={{
                fontSize: "4em",
                marginTop: 0,
                position: "relative",
                top: "-15px",
              }}
            >
              AlfaGo
            </h2>
          </div>
          <div
            className="col-xs-4"
            style={{
              margin: "auto",
              position: "relative",
              bottom: "20px",
            }}
          >
            <RRD.Link to="/alfago">
              <img src="assets/home-start-button.png" />
            </RRD.Link>
          </div>
        </div>

        <div style={{ marginTop: "70px" }}>
          <div id="feat-alfago" className="row">
            <div className="col-sm-6">
              <img src="assets/home-feat-1.png" />
            </div>
            <div className="col-sm-6">
              <h1>AlfaGo</h1>
              <p>
                AlfaGo merupakan store otomasi pertama di Indonesia yang
                tersebar di 34 provinsi dimana store ini terintegrasi dengan
                mobile phone anda sehingga perbelanjaan anda akan dimudahkan
                dengan hanya tap and go tanpa proses kasir.
              </p>

              <p>
                AlfaGo mempunyai visi membawa kemudahan berbelanja dengan satu
                perangkat untuk memenuhi kebutuhan pribadi anda.
              </p>
            </div>
          </div>

          <div id="feat-alfaworldwide" className="row" style={{ marginTop: "80px" }}>
            <div className="col-sm-6">
              <h1>AlfaWorldWide</h1>
              <p>
                AlfaWorldWide menyediakan pengiriman barang - barang di Alfacart
                ke luar negri sehingga bisa diakses oleh orang - orang yang ada
                di luar negri. Alfacart berperan sebagai obat rindu untuk anda
                yang merindukan jajanan mapun barang Indonesia
              </p>
            </div>
            <div className="col-sm-6">
              <img src="assets/home-feat-2.png" />
            </div>
          </div>

          <div id="feat-alfapay" className="row" style={{ marginTop: "80px" }}>
            <div className="col-sm-6">
              <img src="assets/home-feat-3.png" />
            </div>
            <div className="col-sm-6">
              <h1>AlfaPay</h1>
              <p>
                AlfaPay merupakan digital payment service dari AlfaCart, yang
                dapat digunakan di AlfaCart, AlfaGo dan Alfamart. AlfaPay akan
                menjadi personal payment untuk memenuhi kebutuhan pribadi anda.
              </p>
            </div>
          </div>
        </div>
      </div>

      <div
        style={{
          background: "url(assets/footer-bg.png)",
          paddingTop: "150px",
          position: "relative",
          top: "-30px",
        }}
      >
        <div className="container">
          <hr />

          <div style={{ display: "inline-block" }}>
            <ul className="list-unstyled list-inline">
              <li className="list-inline-item">
                <a href="https://www.youtube.com/channel/UCj-E9ANupFZVYX-13WHHVLw" target="_blank">
                  <img src="assets/icon-youtube.png" />
                </a>
              </li>
              <li className="list-inline-item">
                <a href="https://twitter.com/alfacartID" target="_blank">
                  <img src="assets/icon-twitter.png" />
                </a>
              </li>
              <li className="list-inline-item">
                <a href="https://www.instagram.com/alfacartID" target="_blank">
                  <img src="assets/icon-instagram.png" />
                </a>
              </li>
            </ul>
          </div>
          <div style={{ display: "inline-block", float: "right" }}>
            <img src="assets/icon-alfa-go.png" />
          </div>
        </div>
      </div>
    </>
  );
};
