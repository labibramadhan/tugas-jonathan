var AlfaGoPage = () => {
  const [nearestActive, setNearestActive] = R.useState(false);
  jQuery(function ($) {
    $('#toggle-button').click(function () {
      $('.item').css("display", "block");
      $('#toggle-button').remove();
    });
  });

  return (
    <>
      <div className="container">
        <div style={{ marginTop: "50px" }}>
          <div style={{ display: "inline-block", verticalAlign: "middle" }}>
            <RRD.Link to="/home">
              <img src="assets/bingo-alfa-go.png" />
            </RRD.Link>
          </div>
          <div style={{ display: "inline-block", float: "right" }}>
            <img src="assets/icon-alfa-go.png" />
          </div>
        </div>

        <div style={{ marginTop: "50px", marginBottom: "20px" }}>
          <h1 style={{ textAlign: "center" }}>Find AlfaGo</h1>
          <a onClick={() => setNearestActive(true)}>
            <img src="assets/alfa-map.png" />
          </a>

          {!nearestActive && (
            <div className="row" style={{ marginTop: "40px" }}>
              <div className="col-sm-4 pt-3">
                <div className="row">
                  <div className="col-3">
                    <img src="assets/find-alfa-go-1.png" />
                  </div>
                  <div className="col-9" style={{ display: "inline-block" }}>
                    <h6 style={{ fontSize: "15px" }}>AlfaGo Plaza Senayan Mall (Jakarta)</h6>
                    <p style={{ fontSize: "13px" }}>
                      Ps Baru Trade Center, Jl Otto Iskandar Tutup jam 17:00 T:
                      0856-9535-6384
                  </p>
                  </div>
                </div>
              </div>
              <div className="col-sm-4 pt-3">
                <div className="row">
                  <div className="col-3">
                    <img src="assets/find-alfa-go-2.png" />
                  </div>
                  <div className="col-9" style={{ display: "inline-block" }}>
                    <h6 style={{ fontSize: "15px" }}>AlfaGo Plaza Senayan Mall (Jakarta)</h6>
                    <p style={{ fontSize: "13px" }}>
                      Ps Baru Trade Center, Jl Otto Iskandar Tutup jam 17:00 T:
                      0856-9535-6384
                  </p>
                  </div>
                </div>
              </div>
              <div className="col-sm-4 pt-3">
                <div className="row">
                  <div className="col-3">
                    <img src="assets/find-alfa-go-3.png" />
                  </div>
                  <div className="col-9" style={{ display: "inline-block" }}>
                    <h6 style={{ fontSize: "15px" }}>AlfaGo Plaza Senayan Mall (Jakarta)</h6>
                    <p style={{ fontSize: "13px" }}>
                      Ps Baru Trade Center, Jl Otto Iskandar Tutup jam 17:00 T:
                      0856-9535-6384
                  </p>
                  </div>
                </div>
              </div>
              <div className="col-sm-4 pt-3">
                <div className="row">
                  <div className="col-3">
                    <img src="assets/find-alfa-go-4.png" />
                  </div>
                  <div className="col-9" style={{ display: "inline-block" }}>
                    <h6 style={{ fontSize: "15px" }}>AlfaGo Plaza Senayan Mall (Jakarta)</h6>
                    <p style={{ fontSize: "13px" }}>
                      Ps Baru Trade Center, Jl Otto Iskandar Tutup jam 17:00 T:
                      0856-9535-6384
                  </p>
                  </div>
                </div>
              </div>
              <div className="col-sm-4 pt-3">
                <div className="row">
                  <div className="col-3">
                    <img src="assets/find-alfa-go-5.png" />
                  </div>
                  <div className="col-9" style={{ display: "inline-block" }}>
                    <h6 style={{ fontSize: "15px" }}>AlfaGo Plaza Senayan Mall (Jakarta)</h6>
                    <p style={{ fontSize: "13px" }}>
                      Ps Baru Trade Center, Jl Otto Iskandar Tutup jam 17:00 T:
                      0856-9535-6384
                  </p>
                  </div>
                </div>
              </div>
              <div className="col-sm-4 pt-3">
                <div className="row">
                  <div className="col-3">
                    <img src="assets/rectangle.png" />
                  </div>
                  <div className="col-9" style={{ display: "inline-block" }}>
                    <h6 style={{ fontSize: "15px" }}>AlfaGo Plaza Senayan Mall (Jakarta)</h6>
                    <p style={{ fontSize: "13px" }}>
                      Ps Baru Trade Center, Jl Otto Iskandar Tutup jam 17:00 T:
                      0856-9535-6384
                  </p>
                  </div>
                </div>
              </div>
              {/* Show More Items */}
              <div className="col-sm-4 pt-3 item hidden-item" style={{ display: "none" }}>
                <div className="row">
                  <div className="col-3">
                    <img src="assets/rectangle.png" />
                  </div>
                  <div className="col-9" style={{ display: "inline-block" }}>
                    <h6 style={{ fontSize: "15px" }}>AlfaGo Plaza Senayan Mall (Jakarta)</h6>
                    <p style={{ fontSize: "13px" }}>
                      Ps Baru Trade Center, Jl Otto Iskandar Tutup jam 17:00 T:
                      0856-9535-6384
                  </p>
                  </div>
                </div>
              </div>
              <div className="col-sm-4 pt-3 item hidden-item" style={{ display: "none" }}>
                <div className="row">
                  <div className="col-3">
                    <img src="assets/rectangle.png" />
                  </div>
                  <div className="col-9" style={{ display: "inline-block" }}>
                    <h6 style={{ fontSize: "15px" }}>AlfaGo Plaza Senayan Mall (Jakarta)</h6>
                    <p style={{ fontSize: "13px" }}>
                      Ps Baru Trade Center, Jl Otto Iskandar Tutup jam 17:00 T:
                      0856-9535-6384
                  </p>
                  </div>
                </div>
              </div>
              <div className="col-sm-4 pt-3 item hidden-item" style={{ display: "none" }}>
                <div className="row">
                  <div className="col-3">
                    <img src="assets/rectangle.png" />
                  </div>
                  <div className="col-9" style={{ display: "inline-block" }}>
                    <h6 style={{ fontSize: "15px" }}>AlfaGo Plaza Senayan Mall (Jakarta)</h6>
                    <p style={{ fontSize: "13px" }}>
                      Ps Baru Trade Center, Jl Otto Iskandar Tutup jam 17:00 T:
                      0856-9535-6384
                  </p>
                  </div>
                </div>
              </div>
              {/* Show More Items End */}
              <div className="col-12 text-center pt-5">
                <button id="toggle-button">Show More</button>
              </div>
            </div>
          )}

          {nearestActive && (
            <div>
              <div className="nearest-location row">
                <div className="col-6">
                  <div className="row">
                    <div className="col-4">
                      <img src="assets/cashier.png" />
                    </div>
                    <div className="col-8 justify-content-center align-self-center">
                      <h6>
                        <h6 style={{ fontSize: "15px" }}>AlfaGo Plaza Senayan Mall (Jakarta)</h6>
                      </h6>
                      <p style={{ fontSize: "13px" }}>
                        Ps Baru Trade Center, Jl Otto Iskandar Tutup jam 17:00 T:
                        0856-9535-6384
                  </p>
                      <div className="row">
                        <div className="col-4">
                          <img src="assets/565400@2x.png" style={{ float: "left", width: "20px" }} />
                          <span style={{ paddingLeft: "10px" }}>0.5 Km</span>
                        </div>
                        <div className="col-3">
                          <img src="assets/1828884@2x.png" style={{ float: "left", width: "20px" }} />
                          <span style={{ paddingLeft: "10px" }}>4.2</span>
                        </div>
                        <div className="col-4">
                          <img src="assets/565349.png" style={{ float: "left", width: "20px" }} />
                          <a
                            id="direction"
                            className="align-self-center"
                            style={{ paddingLeft: "10px", color: "#2196f3", textDecoration: "none" }}
                            href="https://goo.gl/maps/A4vGzzN9BHPiZqd99"
                            target="_blank">
                            Direction
                            </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div
                className="row"
                style={{
                  marginLeft: 0,
                  marginRight: 0,
                  marginTop: "100px",
                  backgroundImage: "url(assets/box-bg.png)",
                  backgroundSize: "contain",
                  backgroundRepeat: "no-repeat",
                }}
              >
                <div className="col-xs-4">
                  <img
                    src="assets/big-bulb.png"
                    style={{
                      position: "relative",
                      bottom: "-70px",
                      left: "80px"
                    }}
                  />
                </div>
                <div
                  className="col-xs-4"
                  style={{
                    color: "white",
                    position: "relative",
                    top: "40px",
                    left: "50px"
                  }}
                >
                  <h1 style={{ fontSize: "5em", marginBottom: 0 }}>Guess &</h1>
                  <h1
                    style={{
                      fontSize: "5em",
                      marginBottom: 0,
                      position: "relative",
                      top: "-20px",
                      left: "180px"
                    }}
                  >
                    Win
                </h1>
                </div>
                <div
                  className="col-xs-4"
                  style={{
                    margin: "auto",
                    position: "relative",
                  }}
                >
                  <RRD.Link to="/guest-and-win">
                    <img src="assets/home-start-button.png" />
                  </RRD.Link>
                </div>
              </div>
            </div>
          )}
        </div>
      </div>

      <div
        style={{
          background: "url(assets/footer-bg.png)",
          paddingTop: "150px",
          position: "relative",
          top: "-30px",
        }}
      >
        <div className="container">
          <hr />

          <div
            style={{
              display: "inline-block",
              display: "table",
              margin: "auto",
            }}
          >
            <ul className="list-unstyled list-inline">
              <li className="list-inline-item">
                <a href="https://www.youtube.com/channel/UCj-E9ANupFZVYX-13WHHVLw" target="_blank">
                  <img src="assets/icon-youtube.png" />
                </a>
              </li>
              <li className="list-inline-item">
                <a href="https://twitter.com/alfacartID" target="_blank">
                  <img src="assets/icon-twitter.png" />
                </a>
              </li>
              <li className="list-inline-item">
                <a href="https://www.instagram.com/alfacartID" target="_blank">
                  <img src="assets/icon-instagram.png" />
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </>
  );
};
