var GuestAndWinPage = () => {
  const [type, setType] = R.useState(false);
  // claim-voucer
  jQuery(function ($) {
    $('.claim-voucer').click(function () {
      $("#exampleModal").modal('hide');
    });
    $('#login-trigger').click(function () {
      $("#modalBerbagiBerkat").modal('hide');
    });
    $('#logged-in-trigger').click(function () {
      $("#modalLogin").modal('hide');
    });
  });

  return (
    <>
      {/* Navbar */}
      <div className="container">
        <div style={{ marginTop: "50px" }}>
          <div style={{ display: "inline-block", verticalAlign: "middle" }}>
            <RRD.Link to="/home">
              <img src="assets/bingo-alfa-go.png" />
            </RRD.Link>
          </div>
          <div style={{ display: "inline-block", float: "right" }}>
            <img src="assets/icon-alfa-go.png" />
          </div>
        </div>
        <div className="pt-5">
          <div className="row">
            <div className="col-12 text-center justify-content-center align-self-center">
              <div>
                <img src="assets/guess-and-win.png" style={{ width: "300px", margin: "auto" }} />

              </div>
              {/* <h1>Guess & Win</h1> */}
            </div>
            <div className="col-12 text-center justify-content-center align-self-center">
              <div className="card" style={{ width: "80%", margin: "auto" }}>
                <img className="card-img-top" src="assets/rectangle-copy-3@2x.png" alt="Card image cap" />
                <div className="card-body">
                  <h5 className="card-title">Roti tawar merupakan makanan yang menjadi konsumsi rutin layaknya nasi. Berapa harga roti tawar sari roti di alfamart?</h5>
                  <div className="row pt-4">
                    <div className="col-6 text-right justify-content-end">
                      <button
                        data-toggle="modal"
                        data-target="#modalJawabanSalah"
                        className="btn btn-primary"
                        style={{ borderRadius: "10px", backgroundColor: "#005cae", borderColor: "#005cae" }}>
                        <span style={{ paddingRight: "15px" }}>A</span>
                        Rp. 3.000,00
                        </button>
                    </div>
                    <div className="col-6 text-left justify-content-start">
                      <button
                        data-toggle="modal"
                        data-target="#exampleModal"
                        className="btn btn-primary"
                        className="btn btn-primary"
                        style={{ borderRadius: "10px", backgroundColor: "#005cae", borderColor: "#005cae" }}>
                        <span style={{ paddingRight: "15px" }}>B</span>
                        Rp. 4.000,00
                        </button>
                    </div>
                  </div>
                  <div className="row pt-3">
                    <div className="col-6 text-right justify-content-end">
                      <button
                        data-toggle="modal"
                        data-target="#modalJawabanSalah"
                        className="btn btn-primary"
                        className="btn btn-primary"
                        style={{ borderRadius: "10px", backgroundColor: "#005cae", borderColor: "#005cae" }}>
                        <span style={{ paddingRight: "15px" }}>C</span>
                        Rp. 5.000,00
                        </button>
                    </div>
                    <div className="col-6 text-left justify-content-start">
                      <button
                        data-toggle="modal"
                        data-target="#modalJawabanSalah"
                        className="btn btn-primary"
                        className="btn btn-primary"
                        style={{ borderRadius: "10px", backgroundColor: "#005cae", borderColor: "#005cae" }}>
                        <span style={{ paddingRight: "15px" }}>D</span>
                        Rp. 6.000,00
                        </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* Modal Bootstrap */}
      <div className="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-body">
              <div className="container">
                {/* Navbar */}
                <div style={{ marginTop: "50px" }}>
                  <div style={{ display: "inline-block", verticalAlign: "middle" }}>
                    <RRD.Link to="/home">
                      <img src="assets/bingo-alfa-go.png" />
                    </RRD.Link>
                  </div>
                  <div style={{ display: "inline-block", float: "right" }}>
                    <img src="assets/icon-alfa-go.png" />
                  </div>
                  {/* End Of Navbar */}
                  <div className="text-center justify-content-center pt-5">
                    <div className="row">
                      <div className="col-12">
                        <h3>WOW KAMU HEBAT!</h3>
                      </div>
                      <div className="col-12">
                        <p style={{ fontWeight: 600, fontSize: "100px" }}>100</p>
                      </div>
                      <div className="col-12">
                        <h3>Point Menunggumu</h3>
                      </div>
                      <div className="col-12">
                        <p>Kehebatan Kamu bisa disalurkan:</p>
                      </div>
                      <div className="col-12 claim-voucer">
                        <a data-toggle="modal"
                          data-target="#modalBerbagiBerkat"
                          style={{ cursor: "pointer" }}
                          onClick={() => setType(true)}
                        >
                          <img src="assets/berbagi-berkat-pake-alfacart.png" />
                        </a>
                      </div>
                      <div className="col-12 claim-voucer">
                        <a data-toggle="modal"
                          data-target="#modalBerbagiBerkat"
                          style={{ cursor: "pointer" }}
                          onClick={() => setType(false)}
                        >
                          <img src="assets/claim-alfa-pay.png" />
                        </a>
                      </div>
                      <div className="col-12 pt-4">
                        <h3>BERBAGI BERKAT DENGAN ALFACART</h3>
                        <p> Program ini adalah program yang dilakukan oleh Alfacart untuk membantu masyarakat yang kurang mampu dengan memberikan bantuan barang - barang yang tersedia di Alfacart. Setiap donasi kamu sangat berarti bagi mereka! Perlakuan kecil berjuta cerita!</p>
                      </div>
                    </div>
                    <div className="col-12">
                      <div className="row">
                        <div className="col-4" style={{marginBottom: "10px"}}>
                          <img src="assets/berbagi1.jpeg" />
                        </div>
                        <div className="col-4" style={{marginBottom: "10px"}}>
                          <img src="assets/berbagi2.jpeg" />
                        </div>
                        <div className="col-4" style={{marginBottom: "10px"}}>
                          <img src="assets/berbagi3.jpeg" />
                        </div>
                        <div className="col-4" style={{marginBottom: "10px"}}>
                          <img src="assets/berbagi4.jpeg" />
                        </div>
                        <div className="col-4" style={{marginBottom: "10px"}}>
                          <img src="assets/berbagi5.jpeg" />
                        </div>
                        <div className="col-4" style={{marginBottom: "10px"}}>
                          <img src="assets/berbagi6.jpeg" />
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
              {/* Footer */}
              <div
                style={{
                  position: "relative",
                }}
              >
                <div className="container">
                  <hr />

                  <div
                    style={{
                      display: "inline-block",
                      display: "table",
                      margin: "auto",
                    }}
                  >
                    <ul className="list-unstyled list-inline">
                      <li className="list-inline-item">
                        <a href="https://www.youtube.com/channel/UCj-E9ANupFZVYX-13WHHVLw" target="_blank">
                          <img src="assets/icon-youtube.png" />
                        </a>
                      </li>
                      <li className="list-inline-item">
                        <a href="https://twitter.com/alfacartID" target="_blank">
                          <img src="assets/icon-twitter.png" />
                        </a>
                      </li>
                      <li className="list-inline-item">
                        <a href="https://www.instagram.com/alfacartID" target="_blank">
                          <img src="assets/icon-instagram.png" />
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              {/* Footer */}
            </div>
          </div>
        </div>
      </div>
      <div
        style={{
          background: "url(assets/footer-bg.png)",
          paddingTop: "150px",
          position: "relative",
          top: "-30px",
        }}
      >
        <div className="container">
          <hr />

          <div
            style={{
              display: "inline-block",
              display: "table",
              margin: "auto",
            }}
          >
            <ul className="list-unstyled list-inline">
              <li className="list-inline-item">
                <a href="https://www.youtube.com/channel/UCj-E9ANupFZVYX-13WHHVLw" target="_blank">
                  <img src="assets/icon-youtube.png" />
                </a>
              </li>
              <li className="list-inline-item">
                <a href="https://twitter.com/alfacartID" target="_blank">
                  <img src="assets/icon-twitter.png" />
                </a>
              </li>
              <li className="list-inline-item">
                <a href="https://www.instagram.com/alfacartID" target="_blank">
                  <img src="assets/icon-instagram.png" />
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      
      <div className="modal fade" id="modalJawabanSalah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-body">
              <img src="assets/wrong.png" style={{display: "table", margin: "auto", marginBottom: "20px", opacity: "0.9"}} />
              <div className="alert alert-danger" role="alert" style={{textAlign: "center"}}>
                Jawaban kamu salah
              </div>
            </div>
          </div>
        </div>
      </div>

      {/* Modal Bootstrap Click */}
      <div className="modal fade" id="modalBerbagiBerkat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-body">
              <div className="col-12">
                <div className="card"
                  style={{ width: "100%", borderRadius: "10px", border: "none", boxShadow: "0 1px 11px 0 rgba(233, 233, 233, 0.7)" }}>
                  <img src="assets/icon-alfa-go.png" style={{ width: "100px", margin: "auto", paddingTop: "40px" }} />
                  <div className="card-body text-center justify-content-center">
                    <p className="card-title text-center pb-2"><span style={{ fontWeight: 800, fontSize: "2rem", paddingRight: "5px" }}>+100</span>Poin</p>
                    <a data-toggle="modal"
                      data-target="#modalLogin"
                      id="login-trigger"
                    >
                      <button
                        className="btn btn-primary text-center"
                        style={{
                          backgroundColor: "#005ca8",
                          borderColor: "#005ca8",
                          borderRadius: "10px",
                          padding: "10px 15px",
                          margin: "auto"
                        }}>
                        Login AlfaCart
                                </button>
                    </a>
                    {type && (
                      <div>
                        <h2 className="pt-4" style={{ fontSize: "16px", fontWeight: 600, paddingBottom: "8px" }}>
                          BERBAGI BERKAT DENGAN ALFACART
                                </h2>
                        <p style={{ paddingBottom: "20px" }}>
                          Perlakuan Kecilmu menghasilkan berjuta cerita.<br/>Terima Kasih.
                                </p>
                      </div>
                    )}
                    {!type && (
                      <div>
                        <h2 className="pt-4" style={{ fontSize: "16px", fontWeight: 600, paddingBottom: "8px" }}>
                          Check Out dan nikmati kerja kerasmu!!
                                </h2>
                        <p style={{ paddingBottom: "20px" }}>
                          YEY VOUCHER ALFAPAY MU AKAN LANGSUNG MASUK KE ALFACART YA!
                                </p>
                      </div>
                    )}

                    {/* Footer */}
                    <div
                      style={{
                        position: "relative",
                      }}
                    >
                      <div className="container">
                        <div
                          style={{
                            display: "inline-block",
                            display: "table",
                            margin: "auto",
                          }}
                        >
                          <ul className="list-unstyled list-inline">
                            <li className="list-inline-item">
                              <a href="https://www.youtube.com/channel/UCj-E9ANupFZVYX-13WHHVLw" target="_blank">
                                <img src="assets/icon-youtube.png" />
                              </a>
                            </li>
                            <li className="list-inline-item">
                              <a href="https://twitter.com/alfacartID" target="_blank">
                                <img src="assets/icon-twitter.png" />
                              </a>
                            </li>
                            <li className="list-inline-item">
                              <a href="https://www.instagram.com/alfacartID" target="_blank">
                                <img src="assets/icon-instagram.png" />
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    {/* Footer */}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* End Modal Bootstrap Click */}

      {/* Modal Bootstrap Login */}
      <div className="modal fade" id="modalLogin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-body">
              <div className="col-12">
                <div className="card">
                  <div className="col-12">
                    <div className="card" style={{ width: "100%", borderRadius: "10px", border: "none", boxShadow: "0 1px 11px 0 rgba(233, 233, 233, 0.7)" }} >
                      {/* Navbar */}
                      <div className="container pt-5">
                        <div className="row">
                          <div className="col-6 align-self-center" style={{ display: "inline-block", verticalAlign: "middle" }}>
                            <RRD.Link to="/home">
                              <img src="assets/bingo-alfa-go.png" style={{ width: "100px", margin: "auto" }} />
                            </RRD.Link>
                          </div>
                          <div className="col-6" style={{ display: "inline-block", float: "right" }}>
                            <img src="assets/icon-alfa-go.png" style={{ width: "100px" }} />
                          </div>
                        </div>
                      </div>

                      {/* End Of Navbar */}
                      <div className="card-body">
                        <h2 className="text-center" style={{ fontWeight: 800 }}>+100<span style={{ fontWeight: 500, fontSize: "10px" }}>POIN</span></h2>
                        <hr />
                        <div className="py-4">
                          <img src="assets/alfa-pay@2x.png" style={{ margin: "auto", width: "80px" }} />
                        </div>
                        <div id="login-form" className="row text-center justify-content-center">
                          <h2 className="text-center">Log In</h2>
                          <p style={{ fontSize: "14px" }}>Untuk tetap terhubung dengan kami, harap masuk dengan nomor telepon dan kata sandi</p>
                          <div className="col-12">
                            <div className="form-group">
                              <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" />
                            </div>
                            <div className="form-group">
                              <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Password" />
                            </div>
                            <RRD.Link to="/logged-in">
                              <button id="logged-in-trigger" className="btn btn-primary" style={{ borderRadius: "10px", backgroundColor: "#005ca8", borderColor: "#005ca8" }}>Submit</button>
                            </RRD.Link>
                          </div>
                          <p style={{ fontSize: "12px", paddingTop: "20px" }}>Belum punya akun? <span style={{ color: "#2196f3" }}>Daftar sekarang</span></p>
                        </div>
                        {/* Footer */}
                        <div
                          style={{
                            position: "relative",
                          }}
                        >
                          <div className="container">
                            <div
                              style={{
                                display: "inline-block",
                                display: "table",
                                margin: "auto",
                              }}
                            >
                              <ul className="list-unstyled list-inline">
                                <li className="list-inline-item">
                                  <a href="https://www.youtube.com/channel/UCj-E9ANupFZVYX-13WHHVLw" target="_blank">
                                    <img src="assets/icon-youtube.png" />
                                  </a>
                                </li>
                                <li className="list-inline-item">
                                  <a href="https://twitter.com/alfacartID" target="_blank">
                                    <img src="assets/icon-twitter.png" />
                                  </a>
                                </li>
                                <li className="list-inline-item">
                                  <a href="https://www.instagram.com/alfacartID" target="_blank">
                                    <img src="assets/icon-instagram.png" />
                                  </a>
                                </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                        {/* Footer */}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* End Modal Bootstrap Login */}
    </>
  );
};
